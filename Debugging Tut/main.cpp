#include <iostream>
using namespace std;

// Increments an integer by one
// eg. fancyIncrement(0) = 1
// eg. fancyIncrement(1) = 2
int fancyIncrement(int a) {
    int i=a;
    while (i==a) {
        i++;
    }
    return i;
}

// Prints the numbers from zero up to the input
// eg. printRange(0):
// 0
// eg. printRange(3):
/*
0
1
2
3
*/
void printRange(unsigned int x) {
    unsigned int i;
    for(i=0; i<x+1; i++)
        cout<<i << endl;
}

// Prints all the pairs of numbers from zero up to the inputs
// e.g. printRangePairs(5,2):
/*
(0,0),(0,1),(0,2)
(1,0),(1,1),(1,2)
(2,0),(2,1),(2,2)
(3,0),(3,1),(3,2)
(4,0),(4,1),(4,2)
(5,0),(5,1),(5,2)
*/
void printRangePairs(unsigned int m, unsigned int n) {
    unsigned int i, j;
    for (i=0; i<m+1; i++) {
        for (j=0; j<n+1; j++) {
            cout << "(" << i << "," << j << ")";
            if (j != n) {
                cout << ",";
            }
        }
        cout << endl;
    }
}

int main() {
    // fancyIncrement
    cout << "Beginning fancyIncrement test..." << endl;
    cout << "fancyIncrement(0) = " << fancyIncrement(0) << endl;
    cout << "fancyIncrement(1) = " << fancyIncrement(1) << endl;
    
    // printRange
    cout << "Beginning printRange test..." << endl;
    cout << "printRange(0)..." << endl;
    printRange(0);
    cout << "printRange(3)..." << endl;
    printRange(3);
    
    // printRangePairs
    printRangePairs(5,2);
}
